import { LoginForm } from "../components/LoginForm";

export function Login() {
  return (
    <div className="login">
      <div className="login__wrapper">
        <div className="login__content">
          <LoginForm />
        </div>
      </div>
    </div>
  )
}
