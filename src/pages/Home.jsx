import { Link } from 'react-router-dom';
import { Carousel } from '../components/Carousel';
import { Footer } from '../layout/Footer';
import { NavigationWhite } from "../layout/NavigationWhite";


export function Home() {
  return (
    <>
      <div className="home">
        <div className="home__wrapper">
          {/* Header */}
          <header className="home__header">
            <nav className="home__navigation">
              <NavigationWhite />

              <section className="home__introduction">
                <div className="home__introduction__content">
                  <div className="home__introduction__content__text">
                    <h2 className="home__introduction__title">
                      &#8220;A la découverte des<br/>
                      visages<br/>
                      de demain.&#8221;
                    </h2>
                    <h3 className="home__introduction__label">
                      Agence de mannequinat pour<br />
                      hommes et femmes.
                    </h3>
                  </div>
                  <figure className="home__introduction__content__media">
                    <img src="images/introduction-banner.jpg" alt="introduction-banner" className="home__introduction__content__image"/>
                  </figure>
                </div>
              </section>
            </nav>
          </header>

          {/* Main */}
          <main className="home__main">

            {/* Explore */}
            <section className="main__explore">
              <div className="main__explore__content">
                <div className="main__explore__content__woman">
                    <div className="main__explore__woman__navigation">
                      <h3 className="main__explore__woman__title">Femmes</h3>
                      <Link className="main__explore__woman__link" to="/">Tatoués</Link>
                      <Link className="main__explore__woman__link" to="/">Plus Sized</Link>
                    </div>
                  <figure className="main__explore__woman_media">
                    <img src="/images/explore-woman.jpg" alt="" className="main__explore__woman__media__image" />
                  </figure>
                </div>

                <div className="main__explore__content__man">
                  <div className="main__explore__man__navigation">
                    <h3 className="main__explore__man__title">Hommes</h3>
                    <Link className="main__explore__man__link" to="/">Tatoués</Link>
                    <Link className="main__explore__man__link" to="/">Plus Sized</Link>
                  </div>
                  <figure className="main__explore__man_media">
                    <img src="/images/explore-man.jpg" alt="" className="main__explore__man__media__image" />
                  </figure>
                </div>
              </div>
            </section>

            {/* Presentation */}
            <section className="main__presentation">
              <div className="main__presentation__content">
                <div className="main__presentation__content__text">
                  <h2 className="main__presentation__content__title">À propos de nous</h2>
                  <p className="main__presentation__content__paragraph">
                    Ea consectetur quis laborum minim magna. Enim ad dolor aute elit sunt ea. Culpa sunt minim irure sunt nulla esse dolore minim. Reprehenderit est mollit deserunt anim. Laborum aliqua mollit est commodo non incididunt excepteur ullamco esse aliquip et. Minim aute nostrud id occaecat id sit sint nulla minim cupidatat aliquip.
                    Ex quis magna voluptate non sit consectetur proident. Lorem ut ea fugiat in sunt proident occaecat ipsum sit excepteur dolore. Enim nostrud labore in tempor nostrud irure ipsum aliqua. Consectetur proident quis voluptate ex sint duis commodo consequat irure fugiat quis eiusmod.
                  </p>
                </div>
                <div className="main__presentation__content__media">
                  <img src="images/presentation-image.jpg" alt="" className="main__presentation__content__media__image" />
                </div>
              </div>
            </section>

            {/* Informations */}
            <section className="main__informations">
              <div className="main__informations__content">
                <div className="main__informations__description">
                  <p className='main__informations__description__text'>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio dicta dolorum eaque vero sit accusamus provident ipsam iure eveniet pariatur. Totam harum nemo delectus minima id? Minus aperiam atque minima!
                    Animi nisi aliquid suscipit ad illum eligendi amet ipsum veritatis porro cum obcaecati ducimus fugiat libero vitae mollitia natus rerum et, officia unde cupiditate omnis? Omnis quae nostrum est nesciunt.
                    Veniam pariatur eiusmod aliqua mollit. Nostrud officia dolore nulla non quis consequat pariatur sint id aliquip do nostrud ea. Irure nostrud labore irure exercitation et laborum adipisicing pariatur ut ut nostrud fugiat id amet.
                  </p>
                </div>
                <div className="home__informations__carousel">
                  <Carousel />
                </div>
              </div>
            </section>
          </main>

          {/* Footer */}
          <Footer />
        </div>
      </div>
    </>
  )
}
