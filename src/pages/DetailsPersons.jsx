import { FetchDetailsPersons } from '../components/FetchDetailsPersons'
import { NavigationBlack } from '../layout/NavigationBlack'

export function DetailsPersons({ model }) {
  return (
    <>
      <div className="details">
        <div className="details__wrapper">
          <header className="details__header">
            <NavigationBlack />
          </header>
          <main>
            <FetchDetailsPersons />
          </main>
          <footer>

          </footer>
        </div>
      </div>
    </>
  )
}
