import { Link } from 'react-router-dom'
import { FetchPersons } from '../components/FetchPersons'
import { NavigationBlack } from '../layout/NavigationBlack'

export function Models() {

  return (
      <div className="models">
        <div className="models__wrapper">
          {/* Header */}
          <header className="models__header">
            <nav className="models__navigation">
              <NavigationBlack />
            </nav>
          </header>

          {/* Main */}
          <main>
            <h1 className="models__title">Nos modèles</h1>

            <div className="models__content">
              <nav className='models__navigation'>
                <Link className="navigation__section__all" to="/">All</Link>
                <Link className="navigation__section__homme" to="/">Hommes</Link>
                <Link className="navigation__section__femme" to="/">Femmes</Link>
              </nav>

              <FetchPersons />
              {/* <div className="models__fetching">
              </div> */}
            </div>
          </main>

          {/* Footer */}
          <footer></footer>
        </div>
      </div>
  )
}
