import React from 'react'
import { RegisterForm } from '../components/RegisterForm'

export function Register() {
  return (
    <div>
      <RegisterForm />
    </div>
  )
}
