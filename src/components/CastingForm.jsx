import { NavigationBlack } from '../layout/NavigationBlack'
// import { useDispatch } from 'react-redux'
import { useForm } from 'react-hook-form'
import { useState } from 'react'
import axios from 'axios'

export function CastingForm() {

  const { register, handleSubmit } = useForm()
  const [post, setPost] = useState([])

  async function addModel(model) {
    const response = await axios.post(process.env.REACT_APP_SERVER_URL + '/api/casting', model)
    setPost([...post, response.data])
  }

  return (
    <>
      <NavigationBlack />
      <div className="casting">
        <div className="casting__wrapper">
          {/* {redirect ? <Navigate to={'/models'} /> : null} */}

          <h2 className="casting__title">Formulaire de candidature</h2>
          <p className="casting__warning">
            * Merci de remplir le formulaire d'inscription, tous les champs sont
            obligatoires.
          </p>

          <form onSubmit={handleSubmit(addModel)}>
            <div className="casting__form__labels">
              <label>
                Prénom et nom :
                <input
                  type="text"
                  name="name"
                  placeholder="Sylvia Barton (Prénom Nom)"
                  {...register('name', { required: true })}
                />
              </label>

              <label>
                Genre :
                <input
                  type="text"
                  name="sexe"
                  placeholder="Homme - Femme"
                  {...register('sexe', { required: true })}
                />
              </label>

              <label>
                Taille :
                <input
                  type="text"
                  name="height"
                  placeholder="140-180"
                  {...register('height', { required: true })}
                />
              </label>

              <label>
                Poids :
                <input
                  type="text"
                  name="weight"
                  placeholder="50-100"
                  {...register('weight', { required: true })}
                />
              </label>

              <label>
                Mensurations :
                <input
                  type="text"
                  name="bust"
                  placeholder="50-100"
                  {...register('bust', { required: true })}
                />
              </label>

              <label>
                Tour de ceinture :
                <input
                  type="text"
                  name="waist"
                  placeholder="50-100"
                  {...register('waist', { required: true })}
                />
              </label>

              <label>
                Tour de hanches :
                <input
                  type="text"
                  name="hips"
                  placeholder="50-100"
                  {...register('hips', { required: true })}
                />
              </label>

              <label>
                Couleur de vos yeux :
                <input
                  type="text"
                  name="eyes"
                  placeholder="Bleu"
                  {...register('eyes', { required: true })}
                />
              </label>

              <label>
                Couleur de vos cheveux :
                <input
                  type="text"
                  name="hair"
                  placeholder="Brun - Brune"
                  {...register('hair', { required: true })}
                />
              </label>

              <label>
                Photo de vous :
                <input
                  type="text"
                  name="photo"
                  placeholder="https://images.unsplash.com/..."
                  {...register('photo', { required: true })}
                />
              </label>

              <label>
                Vos expériences :
                <input
                  type="text"
                  name="experience"
                  placeholder="My experience ..."
                  {...register('experience', { required: true })}
                />
              </label>
            </div>
            <div className="casting__form__container__btn">
              <input className="casting__form__btn" type="submit" />
            </div>
          </form>
        </div>
      </div>
    </>
  )
}
