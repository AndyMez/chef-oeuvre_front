import { useEffect } from "react"

export function Filter(activeGenre, setActiveGenre) {

  useEffect(() => {

  }, [])


  return (
    <div className="filter__container">
      <button className={activeGenre === "Hommes" || "Femmes" ? "active" : ""} onClick={() => setActiveGenre()}>All</button>
      <button className={activeGenre === "Hommes" ? "active" : ""} onClick={() => setActiveGenre()}>Hommes</button>
      <button className={activeGenre === "Femmes" ? "active" : ""} onClick={() => setActiveGenre()}>Femmes</button>
    </div>
  )
}
