import axios from "axios"
import { useEffect, useState } from "react"
import { CastingCards } from "./CastingCards"
import { CastingForm } from "./CastingForm"
import { Filter } from "./Filter"
import { ModelsCards } from "./ModelsCards"


export function FetchPersons() {

  const [model, setModel] = useState([])
  const [casting, setCasting] = useState([])
  const [filtered, setFiltered] = useState([])
  // const [activeGenre, setActiveGenre] = useState([])

  async function fetchModels() {
    const response = await axios.get(
      process.env.REACT_APP_SERVER_URL + '/api/model'
    )
    setModel(response.data)
    setFiltered(response.data)
    console.log(response.data)
  }

  async function fetchCastings() {
    const response = await axios.get(
      process.env.REACT_APP_SERVER_URL + '/api/casting'
    )
    setCasting(response.data)
    console.log(response.data)
  }

  useEffect(() => {
    fetchModels()
    fetchCastings()
  }, [])

  return (
    <>
      {/* Affichage des "cartes" de mes modèles */}
      <div className="models__grid__position">
        {model.map((model) => (
          <ModelsCards key={model.id} model={model} />
        ))}
        {casting.map((model) => (
          <CastingCards key={model.id} model={model} />
        ))}
      </div>
    </>
  )
}


