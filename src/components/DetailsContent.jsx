import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

export function DetailsContent({detail}) {

  const [experiences, setExperiences] = useState();
  const [photos, setPhotos] = useState();
  const { id } = useParams()

  useEffect(() => {
    async function fetch() {
      try {
        const response = await axios.get(
          process.env.REACT_APP_SERVER_URL + '/api/model/' + id
        )
        setExperiences(response.data)

        console.log(response.data)
      } catch (error) {
        console.log(error)
      }
    }
    fetch()
  }, [id])

  return(
    <>
      <div className="details">
        <div className="details__wrapper">
          <header className="details__header">
            {/* <NavigationBlack /> */}
          </header>

          <main className="details__main">
            <div className="details__content">
              <section className="details__person">
                <div className="details__content__left">
                  <h1 className="details__person__name">{detail.name}</h1>
                  <div className="details__person__experiences">

                    {/* Faire une map qui répète mes expériences */}
                    <p className="details__person__experiences__item">{detail.experience.experience}</p>
                    <p className="details__person__experiences__item">{detail.experience.experience}</p>
                    <p className="details__person__experiences__item">{detail.experience.experience}</p>
                  </div>
                </div>
                <div className="details__content__mid">
                  <figure className="details__person__media">
                    <img src={detail.photo.photo} alt="" className="details__person__media__image"/>
                  </figure>
                </div>
                <div className="details__content__right">
                  <div className="details__person__details">
                    <div className="details__persons__details__name">
                      <span className="details__persons__spacing">Weight</span>
                      <span className="details__persons__spacing">Bust</span>
                      <span className="details__persons__spacing">Waist</span>
                      <span className="details__persons__spacing">Hips</span>
                      <span className="details__persons__spacing">Eyes</span>
                      <span className="details__persons__spacing">Hair</span>
                    </div>
                    <div className="details__persons__details__value">
                      <span className="details__persons__spacing">{detail.weight +' cm'}</span>
                      <span className="details__persons__spacing">{detail.bust +' cm'}</span>
                      <span className="details__persons__spacing">{detail.waist +' cm'}</span>
                      <span className="details__persons__spacing">{detail.hips +' cm'}</span>
                      <span className="details__persons__spacing">{detail.eyes}</span>
                      <span className="details__persons__spacing">{detail.hair}</span>
                    </div>
                  </div>
                </div>
              </section>

              <section className="section__person__more">
                <h2>Plus de photos de Jessica.</h2>
                <div>
                  <span>Image 1</span>
                  <span>Image 2</span>
                  <span>Image 3</span>
                </div>
              </section>
            </div>
          </main>

          <footer className="details__footer"></footer>

        </div>
      </div>
    </>
  )
}
