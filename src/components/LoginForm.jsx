import { useDispatch, useSelector } from "react-redux"
import { useState } from "react";
import { useForm } from "react-hook-form"
import { loginWithCredentials } from "../stores/auth-slice";
import { Navigate } from 'react-router';

export function LoginForm() {

  const { register, handleSubmit } = useForm()

  const [redirect, setRedirect] = useState(false);

  const dispatch = useDispatch()

  const feedback = useSelector(state => state.auth.loginFeedback)

  const onSubmit = (values) => {
    dispatch(loginWithCredentials(values));
    setRedirect(true)
  }

  return (
    <>
      {redirect ? (<Navigate to='/' />) : null}

      <h2>Login</h2>
      <form onSubmit={handleSubmit(onSubmit)}>
        {feedback && <p>{feedback}</p>}

        <label>
          Adresse email :
          <input type="email" placeholder="Insérer votre email" {...register('email')} />
        </label>

        <label>
          Mot de passe
          <input type="password" placeholder ="Votre mot de passe" {...register('password')} />
        </label>

        <input type="submit" value="Envoyer" />
      </form>
    </>
  )
}
