import { Link } from 'react-router-dom'

export function ModelsCards({ model }) {

  const background = `${model.photo.photo}`

  return (
    <div className="models__cards">
      <div className="models__cards__wrapper">
        <div className="models__cards__map">
          <Link to={'/models/' + model.model_id}>
            <figure className="models__cards__media">
              <div className="models__cards__hover__image" style={{ backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${background})` }}>
                <div className="models__cards__hover__content">
                  <h2 className='models__cards__hover__title'>{model.name}</h2>
                  <div className="models__cards__hover__value">
                    <div className="models__cards__hover__name">
                      <span className="models__cards__hover__item">Height</span>
                      <span className="models__cards__hover__item">Weight</span>
                      <span className="models__cards__hover__item">Bust</span>
                      <span className="models__cards__hover__item">Waist</span>
                      <span className="models__cards__hover__item">Hips</span>
                      <span className="models__cards__hover__item">Eyes</span>
                      <span className="models__cards__hover__item">Hair</span>
                    </div>
                    <div className="models__cards__hover__details">
                      <span className="models__cards__hover__item">{model.height + ' cm'}</span>
                      <span className="models__cards__hover__item">{model.weight + ' cm'}</span>
                      <span className="models__cards__hover__item">{model.bust + ' cm'}</span>
                      <span className="models__cards__hover__item">{model.waist + ' cm'}</span>
                      <span className="models__cards__hover__item">{model.hips + ' cm'}</span>
                      <span className="models__cards__hover__item">{model.eyes}</span>
                      <span className="models__cards__hover__item">{model.hair}</span>
                    </div>
                  </div>
                </div>
              </div>
            </figure>

            <h2 className="models__cards__name">{model.name}</h2>
          </Link>
        </div>
      </div>
    </div>
  )
}
