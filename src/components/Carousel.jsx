export function Carousel() {

  return (
    <>
      <div className="carousel">
        <div className="carousel__wrapper">
          <div className="carousel__content">
            <figure className="carousel__media">
              <img src="/images/carousel-1.jpg" alt="" className="carousel__image" />
            </figure>

            <figure className="carousel__media">
              <img src="/images/carousel-2.jpg" alt="" className="carousel__image" />
            </figure>

            <figure className="carousel__media">
              <img src="/images/carousel-3.jpg" alt="" className="carousel__image" />
            </figure>

            <figure className="carousel__media">
              <img src="/images/carousel-4.jpg" alt="" className="carousel__image" />
            </figure>
          </div>
        </div>
      </div>
    </>
  )
}
