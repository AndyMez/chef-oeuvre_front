// import React from 'react';
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { register as registerUser } from '../stores/auth-slice';
import { Navigate } from 'react-router';

export function RegisterForm() {

  const { register, handleSubmit } = useForm();
  const [redirect, setRedirect] = useState(false)
  const dispatch = useDispatch()

  const feedback = useSelector(state => state.auth.registerFeedback)

  const onSubmit = (values) => {
    setRedirect(true)
    dispatch(registerUser(values))
  }

  return (
    <>
      {redirect ? (<Navigate to='/login' />) :null}

      <h2>Register</h2>
      <form onSubmit={handleSubmit(onSubmit)}>
        {feedback && <p>{feedback}</p>}

        <label>Adresse Mail :</label>
        <input type="email" placeholder="Insérer votre email" {...register("email", {required: true})} />

        <label>Mot de passe :</label>
        <input type="password" placeholder ="Doit avoir au moins 3 caractères" {...register("password", {required: true}, {minLength: 3})} />

        <input type="submit" />

      </form>
    </>
  )
}
