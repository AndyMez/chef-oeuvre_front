import axios from 'axios'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { DetailsContent } from './DetailsContent'

export function FetchDetailsPersons(detail) {
  const [details, setDetails] = useState()
  const { id } = useParams()

  useEffect(() => {
    async function fetch() {
      try {
        const response = await axios.get(
          process.env.REACT_APP_SERVER_URL + '/api/model/' + id
        )
        setDetails(response.data)

        console.log(response.data)
      } catch (error) {
        console.log(error)
      }
    }
    fetch()
  }, [id])

  return (
    // Simple fonction qui réalise un .map vers le contenu (Détails) à afficher

    <div>
      {details && (
        <div>
          {details.map((item) => (
            <DetailsContent key={item.id} detail={item} />
          ))}
        </div>
      )}
    </div>
  )
}
