import { createSlice } from '@reduxjs/toolkit'
const initialState = {
  list: [],
}

const userSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    setList(state, { payload }) {
      state.list = payload
    },
  },
})

export const { setList } = userSlice.actions

export default userSlice.reducer
