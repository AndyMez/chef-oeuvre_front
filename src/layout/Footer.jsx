import { Link } from "react-router-dom";
import logo from '../images/LogoBlack.svg';
import instagram from '../images/instagram.svg';
import facebook from '../images/facebook.svg';
import twitter from '../images/twitter.svg';

export function Footer () {
  return (
    <>
      <div className="footer">

        <div className="footer__wrapper">
          <Link to={"/"}>
            <img src={logo} alt="navigation-logo" className="footer__logo" />
          </Link>
          <div className="footer__content">
            <div className="footer__menu">
              <div className="footer__menu__content">
                <h2 className="footer__menu__title">Menu</h2>
                <div className="footer__menu__list">
                  <Link className="footer__menu__list__link" to="/">Accueil</Link>
                  <Link className="footer__menu__list__link" to="/">Homme</Link>
                  <Link className="footer__menu__list__link" to="/">Femme</Link>
                  <Link className="footer__menu__list__link" to="/">Casting</Link>
                  <Link className="footer__menu__list__link" to="/">Contact</Link>
                </div>
              </div>
            </div>

            <div className="footer__support">
              <div className="footer__menu__content">
                <h2 className="footer__menu__title">Support</h2>
                <div className="footer__menu__list">
                  <Link className="footer__menu__list__link" to="/">Terms &#38; Conditions</Link>
                  <Link className="footer__menu__list__link" to="/">Privacy Policy</Link>
                  <Link className="footer__menu__list__link" to="/">Legal Mention</Link>
                </div>
              </div>
            </div>

            <div className="footer__socials">
              <div className="footer__menu__content">
                <h2 className="footer__menu__title">Socials</h2>
                <div className="footer__menu__list__social">
                  <Link className="footer__menu__list__link__social" to="/"><img src={twitter} alt="Twitter" /></Link>
                  <Link className="footer__menu__list__link__social" to="/"><img src={facebook} alt="Facebook" /></Link>
                  <Link className="footer__menu__list__link__social" to="/"><img src={instagram} alt="Instagram" /></Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
