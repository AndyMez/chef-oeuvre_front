import { Link } from 'react-router-dom';
import logo from '../images/LogoWhite.svg';


export function NavigationWhite () {
  return (
    <>
      <div className="navigation">
        <div className="navigation__wrapper">
          <div className="navigation__content">
            <div className="navigation__logo">
              <figure className="navigation__logo__media">
                <Link to={"/"}>
                  <img src={logo} alt="navigation-logo" className="navigation__logo__media__image" />
                </Link>

              </figure>
            </div>

            <div className="navigation__menu">
              {/* <svg width="1.5em" height="1.5em" viewBox="0 0 24 24"><path d="M3 6h18v2H3V6m0 5h18v2H3v-2m0 5h18v2H3v-2z" fill="currentColor"></path></svg> */}
              <div className="navigation__menu__list">
                <Link className="navigation__menu__list__link" to="/models">Mannequins</Link>
                <Link className="navigation__menu__list__link" to="/photographers">Photographes</Link>
                <Link className="navigation__menu__list__link" to="/casting">Candidatures</Link>
                <Link className="navigation__menu__list__link" to="/contact">Contact</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
