import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './styles/index.scss'
import { Home } from './pages/Home'
import { Models } from './pages/Models'
import { Login } from './pages/Login'
import { Register } from './pages/Register'
import { Provider } from 'react-redux'
import { store } from './stores/store'
import { DetailsPersons } from './pages/DetailsPersons'
import { Photographers } from './pages/Photographers'
import { Casting } from './pages/Casting'
import { Contact } from './pages/Contact'

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/models" element={<Models />} />
            <Route path="/models/:id" element={<DetailsPersons />} />
            <Route path="/photographers" element={<Photographers />} />
            <Route path="/casting" element={<Casting />} />
            <Route path="/casting/:id" element={<Casting />} />
            <Route path="/contact" element={<Contact />} />
          </Routes>
        </div>
      </BrowserRouter>
    </Provider>
  )
}

export default App
